Changelog:
==========

**`(pre)` `beta` 0.2.2**
- Change where logs and congfig are kept using `appdirs` module
- Make dist packages (msi, deb, rpm)

**`beta` 0.2.1**
- Add music
- Add config and log files

**`beta` 0.1.0**
- First version