"""Calcifer - A game written in Python and powered by pygame."""
import math
import random
import sys
import json
import pygame

log_file = open("game-log.txt", "a")

data = None
font = None

status = None


class starting:
    pass


class info:
    pass


class intro:
    pass


class title:
    pass


class ending:
    pass


status = starting

while True:

    if status == starting:
        print("Starting...")

        with open("game-data.json", "r") as data_file:
            data = json.load(data_file)

        with open("font.json", "r") as font_file:
            font = json.load(font_file)

        pygame.init()
        pygame.display.set_icon(pygame.image.load("icon.ico"))
        pygame.display.set_caption("Calcifer")

        if data["config"]["fullscreen"]:
            window = pygame.display.set_mode((840, 560), pygame.FULLSCREEN)
        else:
            window = pygame.display.set_mode((840, 560), pygame.DOUBLEBUF)

        window.fill((255, 255, 255))

        pygame.event.get()

        pygame.mixer.music.load("res/music.ogg")
        pygame.mixer.music.play(-1)

        print("Started")

        status = info

    elif status == info:
        window.fill((0, 0, 0))

        events = pygame.event.get()

        for event in events:
            if event.type == pygame.QUIT:
                status = ending

    elif status == intro:
        window.fill((0, 0, 0))

    elif status == title:
        window.fill((0, 0, 0))

    elif status == ending:
        print("Ending...")
        pygame.quit()
        with open("game-data.json", "w") as data_file:
            json.dump(data, data_file, indent=2)
        print("Ended")
        break

    pygame.display.flip()
    pygame.time.Clock().tick(60)

sys.exit(0)
