pyinstaller ^
    --noconsole ^
    --noconfirm ^
    --name="Calcifer" ^
    --distpath="dist/" ^
    --workpath="build/" ^
    --add-data="icon.ico;." ^
    --add-data="res/music.ogg;res" ^
    --add-data="game-data.json;." ^
    --add-data="LICENSE;." ^
    --add-data="README.md;." ^
    --icon="icon.ico" ^
    calcifer.py
